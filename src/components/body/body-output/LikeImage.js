import { Col, Row } from "reactstrap";
import likeImage from "../../../assets/images/like.png";

function LikeImage (props) {
    console.log(props)
    return(
        <>
            <Row className='mt-3'>
                <Col sm={12} md={12} xs={12} lg={12}>
                    {props.outputMessageProp.map((value, index) => {
                        return <p key={index}>{value}</p>
                    })}                    
                </Col>
            </Row>
            <Row className='mt-3'>
                <Col sm={12} md={12} xs={12} lg={12}>
                    {props.likeImgProp ? <img src={likeImage} alt='like' style={{width:"100px"}}/> : null}                        
                </Col>
            </Row>
        </>
    )
}

export default LikeImage;