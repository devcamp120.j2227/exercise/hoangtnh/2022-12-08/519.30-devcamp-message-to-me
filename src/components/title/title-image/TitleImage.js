import { Component } from "react";
import {Row, Col} from "reactstrap"
import background from "../../../assets/images/background.jpg";

class TitleImage extends Component {
    render() {
        return(
            <Row className="mt-3">
                <Col sm={12} md={12} xs={12} lg={12}>
                    <img src={background} style={{width:"500px"}} alt="Background"/>
                </Col>
            </Row>
        )
    }
}

export default TitleImage;