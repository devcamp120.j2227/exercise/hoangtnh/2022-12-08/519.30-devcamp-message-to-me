import "bootstrap/dist/css/bootstrap.min.css";
import { Container } from 'reactstrap';
import Body from "./components/body/Body";
import Title from "./components/title/Title";
import React from 'react';

function App() {
  return (
    <Container className='text-center'>
        <Title />
        <Body />
    </Container>
  );
}

export default App;
